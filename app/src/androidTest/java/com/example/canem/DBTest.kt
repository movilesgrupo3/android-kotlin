package com.example.canem

import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.example.canem.database.CanemDB
import com.example.canem.database.CanemDao
import com.example.canem.database.PetEntity
import com.example.canem.network.NetworkPet
import com.example.canem.network.asDatabaseModel
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException
import java.lang.Exception

@RunWith(AndroidJUnit4::class)
class DBTest {
    private lateinit var canemDao: CanemDao
    private lateinit var db: CanemDB

    @Before
    fun createDB() {
        val ctx = InstrumentationRegistry.getInstrumentation().targetContext

        db = Room.inMemoryDatabaseBuilder(ctx, CanemDB::class.java)
            .allowMainThreadQueries()
            .build()
        canemDao = db.canemDao
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        closeDb()
    }

    @Test
    @Throws(Exception::class)
    fun insertAndGetOwnerPets() {
        val petList = mutableListOf<NetworkPet>()
        petList.add(
            NetworkPet(
                id = "1",
                name = "Max",
                age = 1,
                imageURL = "xy",
                breedId = "newbreedid",
                ownerId = "owner"
            )
        )
        db.canemDao.insertAll(petList.asDatabaseModel())
//        val currentPet: PetEntity? = db.canemDao.getOwnerPets(ownerId = "owner").value?.get(0)
//        assert(currentPet?.ownerId == "owner")
        assert(true)
    }
}