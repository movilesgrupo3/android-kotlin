package com.example.canem.preferences

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager

private const val TOKEN_KEY = "token_key"
private const val UID_KEY = "uid_key"
private const val OID_KEY = "oid_key"
private const val WALK_KEY = "walk_key"

object PreferenceProvider {
    private lateinit var preferences: SharedPreferences

    fun init(context: Context) {
        val appContext = context.applicationContext
        preferences = PreferenceManager.getDefaultSharedPreferences(appContext)
    }

    // TOKEN
    fun setToken(token: String) {
        preferences.edit().putString(
                TOKEN_KEY,
                token
        ).apply()
    }
    fun getToken(): String? {
        return preferences.getString(TOKEN_KEY, null)
    }

    //UID
    fun setUid(uid: String) {
        preferences.edit().putString(
            UID_KEY,
            uid
        ).apply()
    }
    fun getUid(): String? {
        return preferences.getString(UID_KEY, null)
    }

    // OWNER ID
    fun setOwnerId(oid: String) {
        preferences.edit().putString(
            OID_KEY,
            oid
        ).apply()
    }
    fun getOwnerId(): String? {
        return preferences.getString(OID_KEY, null)
    }

    // WALK ID
    fun setWalkId(walkId: String) {
        preferences.edit().putString(
            WALK_KEY,
            walkId
        ).apply()
    }
    fun getWalkId(): String? {
        return preferences.getString(WALK_KEY, null)
    }

    // CLEAR ALL
    fun clearAll() {
        with(preferences.edit()) {
            clear()
            apply()
        }
    }
}