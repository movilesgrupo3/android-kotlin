package com.example.canem.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.example.canem.database.CanemDB
import com.example.canem.database.asDomainModel
import com.example.canem.domain.Pet
import com.example.canem.network.ApiServiceInstance
import com.example.canem.network.NetworkPet
import com.example.canem.network.asDatabaseModel
import com.example.canem.preferences.PreferenceProvider
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class CanemRepository(private val db: CanemDB) {
    private var ownerId: String? = null
    var pets: LiveData<MutableList<Pet>>

    init {
        ownerId = PreferenceProvider.getOwnerId()
        pets = Transformations.map(db.canemDao.getOwnerPets(ownerId.toString())) {
            it.asDomainModel()
        }
    }

    suspend fun refreshPets() {
        withContext(Dispatchers.IO) {
            val token = PreferenceProvider.getToken()
            val networkPets: MutableList<NetworkPet> = ApiServiceInstance
                .retrofitService
                .getOwnerPets(token = "Bearer $token")
            db.canemDao.insertAll(networkPets.asDatabaseModel())
        }
    }

    suspend fun addPet(pet: Pet) {
        withContext(Dispatchers.IO) {
            db.canemDao.insertOne(pet.asDatabaseModel())
        }
    }

    suspend fun removePet(pet: Pet) {
        withContext(Dispatchers.IO) {
            db.canemDao.deleteOne(pet.asDatabaseModel())
        }
    }
}