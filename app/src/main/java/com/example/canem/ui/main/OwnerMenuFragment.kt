package com.example.canem.ui.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.example.canem.R
import com.example.canem.databinding.FragmentMenuBinding
import com.example.canem.nav_fragments.owner.*
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.messaging.FirebaseMessaging

class OwnerMenuFragment : Fragment() {
    // Attrs
    private lateinit var binding: FragmentMenuBinding

    // LifeCycle

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_menu, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val bottomNavView: BottomNavigationView = binding.bottomNavigationOwner
        val navHostFragment: NavHostFragment = childFragmentManager.findFragmentById(R.id.nav_host_owner) as NavHostFragment
        val navController: NavController = navHostFragment.navController

        bottomNavView.setupWithNavController(navController)
        // Unsubscribe from topic
        FirebaseMessaging.getInstance().unsubscribeFromTopic("newWalk")
    }
}