package com.example.canem.ui.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.canem.network.ApiServiceInstance
import com.example.canem.network.NetworkOwner
import com.example.canem.network.UserBody
import com.example.canem.network.firebase_auth.AuthBody
import com.example.canem.network.firebase_auth.AuthResponse
import com.example.canem.network.firebase_auth.AuthServiceInstance
import com.example.canem.network.firebase_roles.RolesBody
import com.example.canem.network.firebase_roles.RolesResponse
import com.example.canem.network.firebase_roles.RolesServiceInstance
import com.example.canem.preferences.PreferenceProvider
import kotlinx.coroutines.launch
import java.lang.Exception

enum class Status {
    LOADING,
    SUCCESS,
    ERROR
}

class MainViewModel : ViewModel() {

    // Attrs

    // Credentials
    private val _email = MutableLiveData<String>()
    val email: LiveData<String> = _email
    private val _password = MutableLiveData<String>()
    val password: LiveData<String> = _password
    private val _role = MutableLiveData<String>()
    val role: LiveData<String> = _role

    // State
    private val _localId = MutableLiveData<String?>()
    val localId: LiveData<String?> = _localId
    private val _authStatus = MutableLiveData<AuthResponse>() // Firebase Auth
    private val _roleStatus = MutableLiveData<RolesResponse>() // Firebase Role
    private val _owner = MutableLiveData<NetworkOwner>()
    val owner: LiveData<NetworkOwner> = _owner

    // Status
    private val _fetched = MutableLiveData<Status>()
    val fetched: LiveData<Status> = _fetched

    // Aux Methods

    fun login(email: String, password: String, signUp: Boolean, name: String?, role: String?) {
        viewModelScope.launch {
            _fetched.value = Status.LOADING
            try {
                // Get tokens
                val authBody = AuthBody(email, password, true)
                if (!signUp) {
                    _authStatus.value = AuthServiceInstance.retrofitService
                        .signIn(authBody)
                } else {
                    _authStatus.value = AuthServiceInstance.retrofitService
                        .signUp(authBody)
                }
                _email.value = email
                _password.value = password
                val token: String = _authStatus.value?.idToken.toString()
                PreferenceProvider.setToken(token)
                val uid: String = _authStatus.value?.localId.toString()
                PreferenceProvider.setUid(uid)
                _localId.value = uid

                if (signUp) { // SIGN UP
                    _role.value = role!!
                    val body = UserBody(
                        name!!, email, uid
                    )
                    if (role == "Owner") {
                        // Create owner in backend
                        _owner.value = ApiServiceInstance.retrofitService.createOwner(body)
                        PreferenceProvider.setOwnerId(_owner.value?.id.toString())
                    } else if (role == "Walker") {
                        // TODO: Assign walker response to variable and save id
                        // Create walker in backend
                        ApiServiceInstance.retrofitService.createWalker(body)
                    }
                    // Set firebase role
                    RolesServiceInstance.retrofitService.setRole(
                        userId = uid,
                        RolesBody(role)
                    )
                } else { // LOGIN
                    // Get role
                    _roleStatus.value = RolesServiceInstance.retrofitService
                        .getRole(uid)
                    _role.value = _roleStatus.value?.role.toString()

                    // Get owner
                    if (_role.value == "Owner") {
                        _owner.value = ApiServiceInstance.retrofitService.getOwner(uid)
                        PreferenceProvider.setOwnerId(_owner.value?.id.toString())
                    } else if (_role.value == "Walker") {
                        // TODO: Get walker info from back
                    }
                }
                _fetched.value = Status.SUCCESS
            } catch (e: Exception) {
                Log.e("ERROR", e.message + " ")
                _fetched.value = Status.ERROR
            }
        }
    }

    fun substringEmail() : String {
        return _email.value.toString().substringBefore("@")
    }
}