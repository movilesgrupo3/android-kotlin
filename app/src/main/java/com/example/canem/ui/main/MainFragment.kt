package com.example.canem.ui.main

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.transition.TransitionManager
import com.example.canem.R
import com.example.canem.databinding.MainFragmentBinding
import com.example.canem.utils.Utils
import com.google.android.material.progressindicator.LinearProgressIndicator

class MainFragment : Fragment() {
    private lateinit var binding: MainFragmentBinding
    private val viewModel: MainViewModel by activityViewModels()

    // LifeCycle
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.main_fragment, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Set role names
        val adapter = ArrayAdapter(requireContext(), R.layout.breed_item, listOf("Owner", "Walker"))
        binding.roleTxt.setAdapter(adapter)

        // Set listeners
        binding.loginButton.setOnClickListener{ onLoginClick() } // Login on click listener
        binding.signUp.setOnCheckedChangeListener{ _, checked -> // On sign up checked changed
            TransitionManager.beginDelayedTransition(binding.main)
            if (checked) {
                binding.nameLayout.visibility = View.VISIBLE
                binding.roleLayout.visibility = View.VISIBLE
                binding.loginButton.text = "SIGN UP"
                binding.nameLayout.requestFocus()
            } else {
                binding.nameLayout.visibility = View.GONE
                binding.roleLayout.visibility = View.GONE
                binding.loginButton.text = "LOGIN"
                binding.loginEmail.requestFocus()
            }
        }
        binding.vm = viewModel // Bind view model
    }

    // Aux Methods
    private fun goToNextPage() {
        Utils.makeToast(requireContext(), "Successful login")
        if (viewModel.role.value.equals("Owner", true))
            findNavController().navigate(R.id.action_mainFragment_to_ownerMenuFragment)
        else
            findNavController().navigate(R.id.action_mainFragment_to_walkerMenuFragment)
    }

    private fun onLoginClick() {
        // Check internet connection
        if (!Utils.internetConn(requireContext())) {
            Utils.showNeutralDialog(
                    requireContext(),
                    "Connection Error",
                    "You currently don't have internet access",
                    "OK"
            )
            return
        }
        // Attempt login
        val email: String = binding.loginEmail.text.toString()
        val password: String = binding.loginPassword.text.toString()
        if (!binding.signUp.isChecked) {
            if (email.isNullOrEmpty() || password.isNullOrEmpty()) {
                Utils.makeToast(requireContext(), "Required field is empty")
                return
            }
            viewModel.login(email, password, false, null, null)
        } else { // Attempt sign in
            val name: String = binding.nameTxt.text.toString()
            val role: String = binding.roleTxt.text.toString()
            if (email.isNullOrEmpty() || password.isNullOrEmpty() || name.isNullOrEmpty() || role.isNullOrEmpty()) {
                Utils.makeToast(requireContext(), "Required field is empty")
                return
            }
            viewModel.login(email, password, true, name, role)
        }
        viewModel.fetched.observe(viewLifecycleOwner, {
            when (it) {
                Status.SUCCESS -> {
                    goToNextPage()
                }
                Status.ERROR -> {
                    binding.authProgress.visibility = LinearProgressIndicator.GONE
                    Utils.makeToast(requireContext(), "Failed to Log in")
                    binding.loginButton.isEnabled = true
                }
                Status.LOADING -> {
                    binding.authProgress.visibility = LinearProgressIndicator.VISIBLE
                    binding.loginButton.isEnabled = false
                }
                else -> {}
            }
        })
    }
}