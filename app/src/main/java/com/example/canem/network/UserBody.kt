package com.example.canem.network

data class UserBody(
    val name: String,
    val email: String,
    val uid: String
)