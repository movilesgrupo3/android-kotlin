package com.example.canem.network

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*

private const val BASE_URL = "https://canem-backend.herokuapp.com/api/"

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .baseUrl(BASE_URL)
    .build()

interface ApiService {
    // Owner
    @POST("owners")
    suspend fun createOwner(@Body ownerBody: UserBody): NetworkOwner
    @GET("walks/ownerWalks")
    suspend fun getOwnerWalks(@Header("Authorization") token: String): List<Walk>
    @GET("owners/ownerPets")
    suspend fun getOwnerPets(@Header("Authorization") token: String): MutableList<NetworkPet>
    @GET("owners/uid/{uid}")
    suspend fun getOwner(@Path("uid") uid: String): NetworkOwner

    // Walker
    @POST("walkers")
    suspend fun createWalker(@Body walkerBody: UserBody): OwnerSearch
    @PUT("walkers/{id}")
    suspend fun updateWalker(
        @Path("id") id: String,
        @Body walkerBody: WalkerUpdate): OwnerSearch
    @GET("walkers/uid/{uid}")
    suspend fun getWalkerByUid(@Path("uid") uid: String): OwnerSearch
    @GET("walks/walkerWalks")
    suspend fun getWalkerWalks(@Header("Authorization") token: String): List<WalkerHistorical>


    @GET("walkers/nearwalkers/{ownerPosition}")
    suspend fun getNearWalkers(@Path("ownerPosition") ownerPosition: String): List<OwnerSearch>

    // Favorite Walkers
    @GET("owners/ownerFavoriteWalkers")
    suspend fun getFavoriteWalkers(@Header("Authorization") token: String): List<OwnerFavorites>

    // Breeds
    @GET("breeds")
    suspend fun getAllBreeds(): List<Breed>

    // Pets
    @POST("dogs/createw_token")
    suspend fun createDog(
        @Header("Authorization") token: String,
        @Body dogBody: CreateDogBody
    ): NetworkPet
    @DELETE("dogs/{dogId}")
    suspend fun deleteDog(@Path("dogId") dogId: String)

    // Walks
    @POST("walks")
    suspend fun createWalk(@Body walksBody: CreateWalkBody): Walk
    @PUT("walks/{walkId}")
    suspend fun updateWalk(
        @Body updateWalkBody: UpdateWalkBody,
        @Path("walkId") walkId: String
    )
}

// Singleton
object ApiServiceInstance {
    val retrofitService: ApiService by lazy {
        retrofit.create(ApiService::class.java)
    }
}