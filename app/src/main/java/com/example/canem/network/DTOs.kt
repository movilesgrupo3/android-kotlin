package com.example.canem.network

import com.example.canem.database.PetEntity
import com.example.canem.domain.Pet
import java.io.Serializable

// Pet
data class NetworkPet (
    val id: String,
    val name: String,
    val age: Int,
    val imageURL: String?,
    val breedId: String,
    val ownerId: String
): Serializable {
    fun toDomainModel(): Pet {
        return Pet(
            id = this.id,
            name = this.name,
            age = this.age,
            imageURL = this.imageURL,
            breedId = this.breedId,
            ownerId = this.ownerId
        )
    }
}
fun MutableList<NetworkPet>.asDomainModel(): MutableList<Pet> {
    return map {
        Pet(
            id = it.id,
            name = it.name,
            age = it.age,
            imageURL = it.imageURL,
            breedId = it.breedId,
            ownerId = it.ownerId
        )
    }.toMutableList()
}
fun MutableList<NetworkPet>.asDatabaseModel(): MutableList<PetEntity> {
    return map {
        PetEntity(
            id = it.id,
            name = it.name,
            age = it.age,
            imageURL = it.imageURL,
            breedId = it.breedId,
            ownerId = it.ownerId
        )
    }.toMutableList()
}

// Owner
data class NetworkOwner(
    val id: String,
    val name: String,
    val email: String,
    val uid: String,
    val imageURL: String?,
    val neighborhoodGroupId: String?
)
