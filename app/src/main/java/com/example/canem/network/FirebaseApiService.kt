package com.example.canem.network

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.RequestBody
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*

private const val BASE_URL = "https://fcm.googleapis.com/fcm/"

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .baseUrl(BASE_URL)
    .build()

interface FirebaseApiService {
    // Walks
    @POST("send")
    suspend fun sendWalkData(
        @Header("Authorization") tokenAuth: String,
        @Header("Content-Type") tokenContent: String,
        @Body walkNotification: RequestBody)
}

// Singleton
object FirebaseApiServiceInstance {
    val retrofitService: FirebaseApiService by lazy {
        retrofit.create(FirebaseApiService::class.java)
    }
}