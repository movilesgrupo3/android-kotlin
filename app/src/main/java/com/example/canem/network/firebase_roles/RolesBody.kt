package com.example.canem.network.firebase_roles

data class RolesBody(
    val role: String
)