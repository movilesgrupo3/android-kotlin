package com.example.canem.network

data class Breed(
    val id: String?,
    val name: String?
)