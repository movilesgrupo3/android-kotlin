package com.example.canem.network.firebase_auth

data class AuthResponse(
    val idToken: String?,
    val localId: String?,
    val error: String?
)