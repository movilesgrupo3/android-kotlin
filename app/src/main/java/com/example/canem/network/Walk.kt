package com.example.canem.network

data class Walk (
    var id: String?,
    var date: String?,
    var price: Float?,
    var distanceTraveled: Float?,
    var stepsWalked: Int?,
    var time: Float?,
    var status: Float?,
    var createdAt: String?,
    var updatedAt: String?,
    var walkerId: String?,
    var dogId: String?,
    var ownerId: String?,
    var neighborhoodGroupId: String?,
)