package com.example.canem.network.firebase_auth

class AuthBody (
    val email: String,
    val password: String,
    val returnSecureToken: Boolean
)