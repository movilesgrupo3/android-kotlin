package com.example.canem.network.firebase_auth

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.Body
import retrofit2.http.POST

private const val API_KEY = "AIzaSyC2DkzSqcg66wbq3T_Pv3Od8jhwiFMmzPY"
private const val BASE_URL = "https://identitytoolkit.googleapis.com/v1/"

private val moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()

private val retrofit = Retrofit.Builder()
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .baseUrl(BASE_URL)
        .build()

interface AuthService {
    @POST("./accounts:signUp?key=$API_KEY")
    suspend fun signUp(@Body authBody: AuthBody): AuthResponse

    @POST("./accounts:signInWithPassword?key=$API_KEY")
    suspend fun signIn(@Body authBody: AuthBody): AuthResponse
}

object AuthServiceInstance {
    val retrofitService: AuthService by lazy {
        retrofit.create(AuthService::class.java)
    }
}
