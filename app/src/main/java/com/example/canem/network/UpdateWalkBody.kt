package com.example.canem.network

data class UpdateWalkBody(
    var price: Float?,
    var distanceTraveled: Float?,
    var stepsWalked: Int?,
    var status: Float?,
)