package com.example.canem.network

data class OwnerFavorites(
    val id: String?,
    val name: String,
    val email: String,
    val uid: String,
    val identification: String,
    val rating: Float,
    val imageURL: String?,
    val status: Boolean,
    val createdAt: String,
    val updatedAt: String,
)
