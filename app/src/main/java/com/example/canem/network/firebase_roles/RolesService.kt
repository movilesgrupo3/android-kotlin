package com.example.canem.network.firebase_roles

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.PUT
import retrofit2.http.Path

private const val BASE_URL = "https://canem-app-default-rtdb.firebaseio.com/userRoles/"

private val moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()

private val retrofit = Retrofit.Builder()
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .baseUrl(BASE_URL)
        .build()

interface RolesService {
    @GET("{userId}.json")
    suspend fun getRole(@Path("userId")userId: String): RolesResponse

    @PUT("{userId}.json")
    suspend fun setRole(
        @Path("userId") userId: String,
        @Body body: RolesBody
    ): RolesResponse
}

object RolesServiceInstance {
    val retrofitService: RolesService by lazy {
        retrofit.create(RolesService::class.java)
    }
}
