package com.example.canem.network

data class CreateWalkBody (
    var price: Float?,
    var distanceTraveled: Float?,
    var stepsWalked: Int?,
    var time: Float?,
    var status: Float?,
    var dogId: String?,
    var walkerId: String?,
    var ownerId: String?,
    var neighborhoodGroupId: String?,
)