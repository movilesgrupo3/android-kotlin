package com.example.canem.network

data class WalkerHistorical(
    val id: String?,
    val date: String,
    val price: Float?,
    val distanceTraveled: Float?,
    val stepsWalked: Int?,
    val time: Int?,
    val status: Int?,
    val dogId: String?,
    val ownerId: String?,
    val neighborhoodGroupId: String?
)
