package com.example.canem.network

class CreateDogBody (
    val name: String,
    val age: Int,
    val imageURL: String?,
    val breedId: String
)