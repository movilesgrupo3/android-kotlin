package com.example.canem

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.canem.database.initDB
import com.example.canem.preferences.PreferenceProvider

class MainActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        // Init preferences and DB
        PreferenceProvider.init(this)
        initDB(application)
    }
}