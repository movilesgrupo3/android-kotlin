package com.example.canem.domain

import com.example.canem.database.PetEntity

data class Pet(
    val id: String,
    val name: String,
    val age: Int,
    val imageURL: String?,
    val breedId: String,
    val ownerId: String
) {
    fun asDatabaseModel(): PetEntity {
        return PetEntity(
            id = this.id,
            name = this.name,
            age = this.age,
            imageURL = this.imageURL,
            breedId = this.breedId,
            ownerId = this.ownerId
        )
    }
}