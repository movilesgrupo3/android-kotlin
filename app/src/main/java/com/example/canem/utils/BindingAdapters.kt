package com.example.canem

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView
import com.example.canem.domain.Pet
import com.example.canem.nav_fragments.owner.OwnerFavoritesAdapter
import com.example.canem.nav_fragments.owner.OwnerSearchAdapter
import com.example.canem.nav_fragments.owner.PetAdapter
import com.example.canem.nav_fragments.owner.OwnerHistoricalAdapter
import com.example.canem.nav_fragments.walker.WalkerHistoricalAdapter
import com.example.canem.network.*
import com.example.canem.ui.main.Status
import com.google.android.material.floatingactionbutton.FloatingActionButton

// Owner Favorites
@BindingAdapter("favoriteWalkersData")
fun bindFavoriteWalkersRV(recyclerView: RecyclerView, data: List<OwnerFavorites>?) {
    val adapter = recyclerView.adapter as OwnerFavoritesAdapter
    adapter.submitList(data)
}

@BindingAdapter("favoriteWalkersStatus")
fun bindFavoriteWalkersStatus(statusImageView: ImageView, status: Status?) {
    when (status) {
        Status.LOADING -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.loading_animation)
        }
        Status.ERROR -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.ic_connection_error)
        }
        Status.SUCCESS -> {
            statusImageView.visibility = View.GONE
        }
    }
}


// Owner Search
@BindingAdapter("walkersData")
fun bindWalkersRV(recyclerView: RecyclerView, data: List<OwnerSearch>?) {
    val adapter = recyclerView.adapter as OwnerSearchAdapter
    adapter.submitList(data)
}

@BindingAdapter("walkersStatus")
fun bindWalkersStatus(statusImageView: ImageView, status: Status?) {
    when (status) {
        Status.LOADING -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.loading_animation)
        }
        Status.ERROR -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.ic_connection_error)
        }
        Status.SUCCESS -> {
            statusImageView.visibility = View.GONE
        }
    }
}

// Owner Historical

@BindingAdapter("ownerHistoricalData")
fun bindOwnerHistoricalRV(recyclerView: RecyclerView, data: List<Walk>?) {
    val adapter = recyclerView.adapter as OwnerHistoricalAdapter
    adapter.submitList(data)
}

@BindingAdapter("ownerHistoricalStatus")
fun bindOwnerHistoricalStatus(statusImageView: ImageView, status: Status?) {
    when (status) {
        Status.LOADING -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.loading_animation)
        }
        Status.ERROR -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.ic_connection_error)
        }
        Status.SUCCESS -> {
            statusImageView.visibility = View.GONE
        }
    }
}

// Owner Pets

// Owner Pets
@BindingAdapter("petData")
fun bindPetRV(recyclerView: RecyclerView, data: LiveData<MutableList<Pet>>?) {
    val adapter = recyclerView.adapter as PetAdapter
    adapter.submitList(data?.value)
}

@BindingAdapter("petStatus")
fun bindPetStatus(statusImageView: ImageView, status: Status?) {
    when (status) {
        Status.LOADING -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.loading_animation)
        }
        Status.ERROR -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.ic_connection_error)
        }
        Status.SUCCESS -> {
            statusImageView.visibility = View.GONE
        }
    }
}

// Walker Historical
@BindingAdapter("walkerHistoricalData")
fun bindWalkerHistoricalRV(recyclerView: RecyclerView, data: List<WalkerHistorical>?) {
    val adapter = recyclerView.adapter as WalkerHistoricalAdapter
    adapter.submitList(data)
}

@BindingAdapter("walkerHistoricalStatus")
fun bindWalkerHistoricalStatus(statusImageView: ImageView, status: Status?) {
    when (status) {
        Status.LOADING -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.loading_animation)
        }
        Status.ERROR -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.ic_connection_error)
        }
        Status.SUCCESS -> {
            statusImageView.visibility = View.GONE
        }
    }
}

@BindingAdapter("addPetVisibility")
fun bindAddPetVisibility(fab: FloatingActionButton, status: Status?) {
    when (status) {
        Status.LOADING -> {
            fab.visibility = FloatingActionButton.GONE
            fab.hide()
        }
        Status.ERROR -> {
            fab.visibility = FloatingActionButton.GONE
            fab.hide()
        }
        Status.SUCCESS -> {
            fab.visibility = FloatingActionButton.VISIBLE
            fab.show()
        }
    }
}