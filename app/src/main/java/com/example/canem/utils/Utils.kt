@file:Suppress("DEPRECATION")

package com.example.canem.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.widget.Toast
import com.google.android.material.dialog.MaterialAlertDialogBuilder

object Utils {
    fun makeToast(context: Context, text: String) {
        Toast.makeText(context, text, Toast.LENGTH_LONG).show()
    }

    fun internetConn(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true
        return isConnected
    }

    fun showNeutralDialog(
            context: Context,
            title: String,
            msg: String,
            neutralBtn: String
    ) {
        MaterialAlertDialogBuilder(context)
                .setTitle(title)
                .setMessage(msg)
                .setNeutralButton(neutralBtn) { _,_ ->
                    //
                }
                .show()
    }

    fun formatDateStr(date: String): String {
        var splitted: List<String> = date.split("T")
        val secSplit: String = splitted[1].subSequence(0, 5).toString()
        return "${splitted[0].replace('-', '/')} - $secSplit"
    }

    fun mapStatus(status: Float): String {
        return when (status.toInt()) {
            0 -> "Waiting for walker confirmation"
            1 -> "In progress"
            2 -> "Finished"
            else -> "Unavailable"
        }
    }
}