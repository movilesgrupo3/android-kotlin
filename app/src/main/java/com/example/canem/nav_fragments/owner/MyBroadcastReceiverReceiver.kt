package com.example.canem.nav_fragments.owner

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class MyBroadcastReceiverReceiver: BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        val i = Intent(context?.applicationContext, RateWalkActivity::class.java)
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        context?.startActivity(i)
    }
}