package com.example.canem.nav_fragments.walker

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.canem.network.*
import com.example.canem.preferences.PreferenceProvider
import com.example.canem.ui.main.Status
import kotlinx.coroutines.launch
import okhttp3.RequestBody
import java.lang.Exception

class WalkerViewModel: ViewModel() {
    // State
    private val _historical = MutableLiveData<List<WalkerHistorical>>()
    val historical: LiveData<List<WalkerHistorical>> = _historical
    private val _walkerUpdateResponse = MutableLiveData<OwnerSearch>()
    val walkerUpdateResponse = _walkerUpdateResponse
    private val _walkerResponse = MutableLiveData<OwnerSearch>()
    val walkerResponse = _walkerResponse
    // Status
    private val _statusHistorical = MutableLiveData<Status>()
    val statusHistorical: LiveData<Status> = _statusHistorical
    private val _updateWalkerStatus = MutableLiveData<Status>()
    val updateWalkerStatus = _updateWalkerStatus
    private val _walkerStatus = MutableLiveData<Status>()
    val walkerStatus = _walkerStatus
    private val _updateWalkStatus = MutableLiveData<Status>()
    val updateWalkStatus: LiveData<Status> = _updateWalkStatus

    private val _statusSendNotification = MutableLiveData<Status>()
    val statusSendNotification: LiveData<Status> = _statusSendNotification



    init {
        getHistorical()
    }

    // Aux Methods

    private fun getHistorical() {
        viewModelScope.launch {
            _statusHistorical.value = Status.LOADING
            try {
                val token = PreferenceProvider.getToken()
                val resultHistorical: List<WalkerHistorical> =
                    ApiServiceInstance.retrofitService.getWalkerWalks(token = "Bearer $token").take(15)
                _historical.value = resultHistorical
                _statusHistorical.value = Status.SUCCESS
            } catch (e: Exception) {
                Log.d("ERROR(Get Historical)", e.message.toString())
                _statusHistorical.value = Status.ERROR
                _historical.value = listOf()
            }
        }
    }

    fun getWalker() {
        _walkerStatus.value = Status.LOADING
        viewModelScope.launch {
            try {
                val uid = PreferenceProvider.getUid()
                val resultWalker = ApiServiceInstance.retrofitService.getWalkerByUid(uid!!)
                _walkerResponse.value = resultWalker
                _walkerStatus.value = Status.SUCCESS
            } catch (e: Exception) {
                Log.e("Error(Get Walker)", e.message.toString())
                _walkerStatus.value = Status.ERROR
            }
        }
    }

    fun updateWalker(walker: WalkerUpdate, walkerID: String) {
        _updateWalkerStatus.value = Status.LOADING
        viewModelScope.launch {
            try {
                val resultWalker = ApiServiceInstance.retrofitService.updateWalker(walkerID,walker)
                _walkerUpdateResponse.value = resultWalker
                _updateWalkerStatus.value = Status.SUCCESS
            } catch (e: Exception) {
                Log.e("Error(Update Walker)", e.message.toString())
                _updateWalkerStatus.value = Status.ERROR
            }
        }
    }

    fun updateWalk(walk: UpdateWalkBody, walkId: String) {
        _updateWalkStatus.value = Status.LOADING
        viewModelScope.launch {
            try {
                ApiServiceInstance.retrofitService.updateWalk(
                    updateWalkBody = walk,
                    walkId = walkId
                )
                _updateWalkStatus.value = Status.SUCCESS
            } catch(e: Exception) {
                Log.e("Error(Update Walk)", e.message.toString())
                _updateWalkStatus.value = Status.ERROR
            }
        }
    }

    fun sendNotification(walkNotification: RequestBody, serviceKey: String) {
        _statusSendNotification.value = Status.LOADING
        viewModelScope.launch {
            try {
                FirebaseApiServiceInstance.retrofitService.sendWalkData(
                    "key=$serviceKey",
                    "application/json",
                    walkNotification
                )
                _statusSendNotification.value = Status.SUCCESS
            } catch (e: Exception) {
                Log.e("Error(Send Walk Data)", e.message.toString())
                _statusSendNotification.value = Status.ERROR
            }
        }
    }
}