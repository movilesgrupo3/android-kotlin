package com.example.canem.nav_fragments.owner

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.canem.databinding.OwnerSearchItemBinding
import com.example.canem.network.OwnerSearch

class OwnerSearchAdapter(fragment: OwnerSearchFragment): ListAdapter<OwnerSearch, OwnerSearchAdapter.OwnerSearchViewHolder>(DiffCallback) {
    val fragment: OwnerSearchFragment = fragment

    class OwnerSearchViewHolder(private var binding: OwnerSearchItemBinding): RecyclerView.ViewHolder(binding.root) {
        val hire: Button = binding.buttonOwnerItem
        fun bind(ownerSearch: OwnerSearch) {
            binding.ownerSearch = ownerSearch
            binding.executePendingBindings()
        }
    }

    companion object DiffCallback: DiffUtil.ItemCallback<OwnerSearch>() {
        override fun areItemsTheSame(oldItem: OwnerSearch, newItem: OwnerSearch): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: OwnerSearch, newItem: OwnerSearch): Boolean {
            return (
                    oldItem.name == newItem.name &&
                    oldItem.email == newItem.email &&
                    oldItem.identification == newItem.identification
            )
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OwnerSearchAdapter.OwnerSearchViewHolder {
        return OwnerSearchViewHolder(
            OwnerSearchItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: OwnerSearchAdapter.OwnerSearchViewHolder, position: Int) {
        val ownerSearch = getItem(position)
        holder.bind(ownerSearch)
        holder.hire.setOnClickListener {
            fragment.createWalk(ownerSearch.id)
        }
    }
}