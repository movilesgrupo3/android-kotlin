package com.example.canem.nav_fragments.owner

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.canem.databinding.OwnerFavoriteWalkersItemBinding
import com.example.canem.network.OwnerFavorites

class OwnerFavoritesAdapter: ListAdapter<OwnerFavorites, OwnerFavoritesAdapter.OwnerFavoritesViewHolder>(DiffCallback) {
    class OwnerFavoritesViewHolder(private var binding: OwnerFavoriteWalkersItemBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(ownerFavorites: OwnerFavorites) {
            binding.ownerFavorites = ownerFavorites
            binding.executePendingBindings()
        }
    }

    companion object DiffCallback: DiffUtil.ItemCallback<OwnerFavorites>() {
        override fun areItemsTheSame(oldItem: OwnerFavorites, newItem: OwnerFavorites): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: OwnerFavorites, newItem: OwnerFavorites): Boolean {
            return (
                    oldItem.name == newItem.name &&
                            oldItem.email == newItem.email &&
                            oldItem.identification == newItem.identification
                    )
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OwnerFavoritesAdapter.OwnerFavoritesViewHolder {
        return OwnerFavoritesViewHolder(
            OwnerFavoriteWalkersItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: OwnerFavoritesAdapter.OwnerFavoritesViewHolder, position: Int) {
        val ownerFavorites = getItem(position)
        holder.bind(ownerFavorites)
    }
}