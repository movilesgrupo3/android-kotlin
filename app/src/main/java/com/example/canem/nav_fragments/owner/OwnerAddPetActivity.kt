package com.example.canem.nav_fragments.owner

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import androidx.activity.viewModels
import androidx.core.widget.doOnTextChanged
import com.example.canem.R
import com.example.canem.databinding.ActivityOwnerAddPetBinding
import com.example.canem.network.Breed
import com.example.canem.network.CreateDogBody
import com.example.canem.network.NetworkPet
import com.example.canem.ui.main.Status
import com.example.canem.utils.Utils
import java.io.Serializable
import java.lang.NumberFormatException

class OwnerAddPetActivity : AppCompatActivity() {
    private lateinit var binding: ActivityOwnerAddPetBinding
    private val vm: AddPetViewModel by viewModels()

    // Lifecycle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOwnerAddPetBinding.inflate(layoutInflater)
        setContentView(binding.root)

        vm.breedStatus.observe(this, {
            when (it) {
                Status.SUCCESS -> {onSuccess()}
                Status.ERROR -> {onError()}
                else -> {}
            }
        })
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        super.onBackPressed()
    }

    // Aux Methods

    private fun onPetCreated(dog: NetworkPet?) {
        val intent = Intent()
        intent.putExtra("dog", dog as Serializable)
        setResult(Activity.RESULT_OK, intent)
        Utils.makeToast(this, "Dog Created!")
        finish()
    }

    private fun createPet(
        name: String,
        age: Int,
        breedId: String
    ) {
        // Check conn
        if (!Utils.internetConn(this)) {
            Utils.showNeutralDialog(
                this,
                "Connection Error",
                "You currently don't have internet access",
                "OK"
            )
            return
        }
        // Attempt dog creation
        val dog = CreateDogBody(
            name = name,
            age = age,
            breedId = breedId,
            imageURL = "www.dog1.com"
        )
        vm.createDog(dog)
        vm.createDogStatus.observe(this, {
            when (it) {
                Status.SUCCESS -> {
                    val dogResponse: NetworkPet? = vm.dogResponse.value
                    onPetCreated(dogResponse)
                }
                Status.ERROR -> {
                    Utils.showNeutralDialog(
                        this,
                        "Error",
                        "Failed to create dog",
                        "OK"
                    )
                }
                else -> {}
            }
        })
    }

    // On state handlers

    private fun onSuccess() {
        val breeds: List<Breed> = vm.breeds.value!!
        val breedNames: List<String> = breeds.map { it.name.toString() }
        val adapter = ArrayAdapter(this, R.layout.breed_item, breedNames)
        binding.breedEditText.setAdapter(adapter)

        // Name Validation
        var nameVal = false
        binding.dogName.editText?.doOnTextChanged { text, _, _, _ ->
            if (text.isNullOrBlank()) {
                binding.dogName.error = "Name is empty"
                nameVal = false
            } else {
                binding.dogName.error = null
                nameVal = true
            }
        }
        // Age Validation
        var ageVal = false
        binding.dogAge.editText?.doOnTextChanged { text, _, _, _ ->
            if (text.isNullOrBlank()) {
                binding.dogAge.error = "Age is empty"
                ageVal = false
            } else {
                try {
                    val age: Int = text.toString().toInt()
                    if (age < 0) {
                        binding.dogAge.error = "Introduce a valid number"
                        ageVal = false
                    } else {
                        binding.dogAge.error = null
                        ageVal = true
                    }
                } catch (e: NumberFormatException) {
                    binding.dogAge.error = "Introduce a valid number"
                }
                binding.dogAge.error = null
            }
        }
        // Breed Validation
        var breedVal = false
        binding.dogBreed.editText?.doOnTextChanged { text,_,_,_ ->
            if (text.isNullOrBlank()) {
                binding.dogBreed.error = "Pick a dog breed"
                breedVal = false
            } else {
                binding.dogBreed.error = null
                breedVal = true
            }
            binding.dogBreed.error = null
        }

        // Add evt listeners
        binding.addPetBtn.setOnClickListener {
            val name: String = binding.dogName.editText?.text.toString()
            val age: String = binding.dogAge.editText?.text.toString()
            val breed: String = binding.dogBreed.editText?.text.toString()
            if (nameVal && ageVal && breedVal) {
                createPet(
                    name,
                    age.toInt(),
                    breeds.filter{ it.name == breed }[0].id.toString()
                )
            } else {
                if (name.isNullOrBlank())
                    binding.dogName.error = "Name is empty"
                if (age.isNullOrBlank())
                    binding.dogAge.error = "Age is empty"
                if (breed.isNullOrBlank())
                    binding.dogBreed.error = "Pick a dog breed"
            }
        }

        // Enable views
        binding.addPetMainContent.visibility = View.VISIBLE
        binding.breedStatusImg.visibility = View.GONE
    }

    private fun onError() {
        binding.breedStatusImg.setImageResource(R.drawable.ic_connection_error)
    }
}