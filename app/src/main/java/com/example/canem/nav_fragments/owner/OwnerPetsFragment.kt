package com.example.canem.nav_fragments.owner

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.activityViewModels
import com.example.canem.databinding.FragmentOwnerPetsBinding
import com.example.canem.domain.Pet
import com.example.canem.network.NetworkPet
import com.example.canem.utils.Utils

class OwnerPetsFragment : Fragment() {
    private lateinit var binding: FragmentOwnerPetsBinding
    private val ownerVM: OwnerViewModel by activityViewModels()

    // Result Contract
    val resultContract = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) { result: ActivityResult? ->
        if (result?.resultCode == Activity.RESULT_OK) {
            val intent: Intent? = result.data
            if (intent != null) {
                val dog: NetworkPet = intent.extras?.get("dog") as NetworkPet
                ownerVM.addDog(dog.toDomainModel())
                binding.recyclerView.adapter?.notifyDataSetChanged()
            }
        }
    }

    // Lifecycle

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentOwnerPetsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = this
        binding.viewModel = ownerVM
        binding.recyclerView.adapter = PetAdapter(this@OwnerPetsFragment)

        binding.floatingActionButton.setOnClickListener {
            if (Utils.internetConn(requireContext())) {
                val intent = Intent(context, OwnerAddPetActivity::class.java)
                resultContract.launch(intent)
            } else {
                Utils.showNeutralDialog(
                    requireContext(),
                    "Connection Error",
                    "You currently don't have internet access",
                    "OK"
                )
            }
        }
    }

    // Aux methods

    fun deleteDog(dog: Pet) {
        ownerVM.deleteDog(dog)
    }
}