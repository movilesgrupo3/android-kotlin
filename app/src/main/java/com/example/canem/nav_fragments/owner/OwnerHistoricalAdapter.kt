package com.example.canem.nav_fragments.owner

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.canem.databinding.OwnerHistoricalItemBinding
import com.example.canem.network.Walk
import com.example.canem.utils.Utils

class OwnerHistoricalAdapter: ListAdapter<Walk, OwnerHistoricalAdapter.OwnerHistoricalViewHolder>(DiffCallback) {
    class OwnerHistoricalViewHolder(private var binding: OwnerHistoricalItemBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(ownerHistorical: Walk) {
            binding.date = Utils.formatDateStr(ownerHistorical.date.toString())
            binding.status = ownerHistorical.status?.let { Utils.mapStatus(it) }
            binding.executePendingBindings()
        }
    }

    companion object DiffCallback: DiffUtil.ItemCallback<Walk>() {
        override fun areItemsTheSame(oldItem: Walk, newItem: Walk): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Walk, newItem: Walk): Boolean {
            return (
                oldItem.date == newItem.date &&
                oldItem.price == newItem.price &&
                oldItem.distanceTraveled == newItem.distanceTraveled &&
                oldItem.stepsWalked == newItem.stepsWalked &&
                oldItem.time == newItem.time &&
                oldItem.status == newItem.status &&
                oldItem.createdAt == newItem.createdAt &&
                oldItem.updatedAt == newItem.updatedAt &&
                oldItem.ownerId == newItem.ownerId &&
                oldItem.neighborhoodGroupId == newItem.neighborhoodGroupId
            )
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OwnerHistoricalViewHolder {
        return OwnerHistoricalViewHolder(
            OwnerHistoricalItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: OwnerHistoricalViewHolder, position: Int) {
        val walk = getItem(position)
        holder.bind(walk)
    }
}