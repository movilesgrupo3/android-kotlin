package com.example.canem.nav_fragments.owner

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.canem.databinding.TransparentActivityBinding
import com.example.canem.utils.Utils

class RateWalkActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = TransparentActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.submitRating.setOnClickListener {
            // Check conn
            if (!Utils.internetConn(this)) {
                Utils.showNeutralDialog(
                    this,
                    "Connection Error",
                    "You currently don't have internet access",
                    "OK"
                )
                return@setOnClickListener
            }
            this@RateWalkActivity.finish()
        }
    }
}