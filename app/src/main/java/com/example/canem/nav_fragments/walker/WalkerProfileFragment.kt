package com.example.canem.nav_fragments.walker

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.example.canem.MainActivity
import com.example.canem.databinding.FragmentWalkerProfileBinding
import com.example.canem.preferences.PreferenceProvider
import com.example.canem.ui.main.MainViewModel


class WalkerProfileFragment : Fragment() {
    private lateinit var binding: FragmentWalkerProfileBinding
    private val viewModel: MainViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentWalkerProfileBinding.inflate(inflater, container, false)
        return binding.root

        //return inflater.inflate(R.layout.fragment_walker_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.apply{
            viewModel = this@WalkerProfileFragment.viewModel
            walkerProfileFragment = this@WalkerProfileFragment
        }
    }


    // AuxMethods
    fun logout() {
        // Clear storage
        viewModelStore.clear()
        PreferenceProvider.clearAll()
        // Go to Login page
        val mainIntent = Intent(requireContext(), MainActivity::class.java)
        startActivity(mainIntent)
        requireActivity().finish()
    }
}