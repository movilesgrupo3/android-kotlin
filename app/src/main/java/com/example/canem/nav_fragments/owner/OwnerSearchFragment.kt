package com.example.canem.nav_fragments.owner

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.canem.databinding.FragmentOwnerSearchBinding
import com.example.canem.domain.Pet
import com.example.canem.network.CreateWalkBody
import com.example.canem.network.Walk
import com.example.canem.preferences.PreferenceProvider
import com.example.canem.ui.main.Status
import com.example.canem.utils.Utils
import com.google.android.gms.location.*
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.messaging.FirebaseMessaging
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONObject


class OwnerSearchFragment : Fragment() {
    private lateinit var binding: FragmentOwnerSearchBinding
    private val viewModel: OwnerViewModel by activityViewModels()
    // Location
    private val PERMISSION_ID = 42
    lateinit var mFusedLocationClient: FusedLocationProviderClient
    private var latitude: String? = null
    private var longitude: String? = null
    
    // Lifecycle

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentOwnerSearchBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // View & Data binding
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        binding.recyclerView.adapter = OwnerSearchAdapter(this@OwnerSearchFragment)
        // Location
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        getLastLocation()
        // Set refresh
        binding.swipeRefresh.setOnRefreshListener {
            // Check conn
            if (!Utils.internetConn(requireContext())) {
                Utils.showNeutralDialog(
                    requireContext(),
                    "Connection Error",
                    "You currently don't have internet access",
                    "OK"
                )
                binding.swipeRefresh.isRefreshing = false
                return@setOnRefreshListener
            }
            // On refresh listener
            if (latitude != null && longitude != null)
                viewModel.getNearWalkers("${latitude};$longitude")
            viewModel.statusWalkers.observe(viewLifecycleOwner, {
                when(it) {
                    Status.LOADING -> {binding.swipeRefresh.isRefreshing = true}
                    Status.ERROR -> {
                        binding.swipeRefresh.isRefreshing = false
                        Utils.makeToast(requireContext(), "Failed to fetch walkers")
                    }
                    else -> {binding.swipeRefresh.isRefreshing = false}
                }
            })
        }
    }

    // Aux Methods

    @RequiresApi(Build.VERSION_CODES.N)
    fun createWalk(walkerId: String?) {
        if (walkerId != null) {
            // Validate conn
            if (!Utils.internetConn(requireContext())) {
                Utils.showNeutralDialog(
                    requireContext(),
                    "Connection Error",
                    "You currently don't have internet access",
                    "OK"
                )
                return
            }
            // Request Pet
            if (viewModel.statusPets.value != Status.SUCCESS) { // Validate pets
                Utils.showNeutralDialog(
                    requireContext(),
                    "Error",
                    "Your pets aren't available yet, try again in a few seconds",
                    "OK"
                )
                return
            }
            val pets: MutableList<Pet>? = viewModel.pets.value
            if (pets != null) {
                if (pets.size == 0) {
                    Utils.showNeutralDialog(
                        requireContext(),
                        "Error",
                        "You still don't have any pets, try adding some",
                        "OK"
                    )
                    return
                }
                val singleItems: Array<String> = pets.map { it.name }.toTypedArray()
                val checkedItem = 0
                var dogId: String? = null
                MaterialAlertDialogBuilder(requireContext())
                    .setTitle("Choose Pet")
                    .setPositiveButton("OK") { _, _ ->
                        // Attempt dog creation
                        val walkBody = CreateWalkBody(
                            price = 0.0F,
                            distanceTraveled = 0.0F,
                            stepsWalked = null,
                            time = null,
                            status = 1.0F,
                            dogId = dogId,
                            walkerId = walkerId,
                            ownerId = PreferenceProvider.getOwnerId(),
                            neighborhoodGroupId = null,
                        )
                        viewModel.createWalk(walkBody)
                        viewModel.statusCreateWalk.observe(this, {
                            when (it) {
                                Status.SUCCESS -> {
                                    val walkResponse: Walk? = viewModel.createWalk.value
                                    onWalkCreated(walkResponse)
                                    FirebaseMessaging.getInstance().subscribeToTopic("endWalk")
                                }
                                Status.ERROR -> {
                                    Utils.showNeutralDialog(
                                        requireContext(),
                                        "Error",
                                        "Failed to create walk",
                                        "OK"
                                    )
                                }
                                else -> {}
                            }
                        })
                    }
                    .setNegativeButton("CANCEL") { dialog, _ ->
                        dialog.cancel()
                    }
                    .setSingleChoiceItems(singleItems, checkedItem) { _, which ->
                        if (which >= 0) {
                            val petName: String = singleItems[which]
                            dogId = pets.filter { it.name == petName }[0].id
                        }
                    }
                    .show()
            } else {
                Utils.showNeutralDialog(
                    requireContext(),
                    "Error",
                    "Your pets aren't available yet, try again in a few seconds",
                    "OK"
                )
                return
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun onWalkCreated(walkResponse: Walk?){
        // Notification
        requireContext().packageManager.getApplicationInfo(requireContext().packageName,PackageManager.GET_META_DATA).apply {
            val serverKey = metaData.getString("FIREBASE_SERVER_KEY")
            val notification = JSONObject()
            val notificationBody = JSONObject()
            notificationBody.put("title", "Walk Found")
            notificationBody.put("message","An owner has hired you! The walk will start soon.")
            notificationBody.put("walkID", walkResponse?.id)

            notification.put("to","/topics/newWalk")
            notification.put("data",notificationBody)
            val body = RequestBody.create(MediaType.parse("application/json"),notification.toString())
            if (serverKey != null) {
                viewModel.sendNotification(body,serverKey)
            }
        }
        // Dialog
        Utils.showNeutralDialog(
            requireContext(),
            "Walk In Progress",
            "Your Walker has started a walk, you will be notified when they're finished.",
            "OK"
        )
        // Remove walker
        viewModel.removeWalker(walkResponse?.walkerId!!)
        binding.recyclerView.adapter?.notifyDataSetChanged()
    }

    // Last Location

    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                mFusedLocationClient.lastLocation.addOnCompleteListener(requireActivity()) { task ->
                    var location: Location? = task.result
                    if (location == null) {
                        requestNewLocationData()
                    } else {
                        latitude = location.latitude.toString()
                        longitude = location.longitude.toString()
                        // Get Walkers
                        val latlng = "${latitude};$longitude"
                        viewModel.getNearWalkers(latlng)
                    }
                }
            } else {
                viewModel.setStatusWalkers(Status.ERROR) // Set fetched walkers error
                Utils.makeToast(requireContext(), "Turn on location")
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
        } else {
            requestPermissions()
        }
    }

    @SuppressLint("MissingPermission")
    private fun requestNewLocationData() {
        var mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 0
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        mFusedLocationClient!!.requestLocationUpdates(
            mLocationRequest, mLocationCallback,
            Looper.myLooper()
        )
    }

    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            var mLastLocation: Location = locationResult.lastLocation
            latitude = mLastLocation.latitude.toString()
            longitude = mLastLocation.longitude.toString()
            // Get Walkers
            val latlng = "${latitude};$longitude"
            viewModel.getNearWalkers(latlng)
        }
    }

    private fun isLocationEnabled(): Boolean {
        var locationManager: LocationManager = requireActivity().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    private fun checkPermissions(): Boolean {
        if (ActivityCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            requireActivity(),
            arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION),
            PERMISSION_ID
        )
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == PERMISSION_ID) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                getLastLocation()
            }
        }
    }
}