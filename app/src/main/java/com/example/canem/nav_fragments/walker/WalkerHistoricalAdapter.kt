package com.example.canem.nav_fragments.walker

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.canem.databinding.WalkerHistoricalItemBinding
import com.example.canem.network.WalkerHistorical
import com.example.canem.utils.Utils

class WalkerHistoricalAdapter: ListAdapter<WalkerHistorical, WalkerHistoricalAdapter.WalkerHistoricalViewHolder>(DiffCallback) {
    class WalkerHistoricalViewHolder(private var binding: WalkerHistoricalItemBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(walkerHistorical: WalkerHistorical) {
            binding.date = Utils.formatDateStr(walkerHistorical.date)
            binding.status = walkerHistorical.status?.let { Utils.mapStatus(it.toFloat()) }
            binding.executePendingBindings()
        }
    }

    companion object DiffCallback: DiffUtil.ItemCallback<WalkerHistorical>() {
        override fun areItemsTheSame(oldItem: WalkerHistorical, newItem: WalkerHistorical): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: WalkerHistorical, newItem: WalkerHistorical): Boolean {
            return (
                oldItem.date == newItem.date &&
                oldItem.price == newItem.price &&
                oldItem.distanceTraveled == newItem.distanceTraveled &&
                oldItem.stepsWalked == newItem.stepsWalked &&
                oldItem.time == newItem.time &&
                oldItem.status == newItem.status &&
                oldItem.dogId == newItem.dogId &&
                oldItem.ownerId == newItem.ownerId &&
                oldItem.neighborhoodGroupId == newItem.neighborhoodGroupId
            )
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WalkerHistoricalViewHolder {
        return WalkerHistoricalViewHolder(
            WalkerHistoricalItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: WalkerHistoricalViewHolder, position: Int) {
        val walkerHistorical = getItem(position)
        holder.bind(walkerHistorical)
    }
}