package com.example.canem.nav_fragments.walker

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.example.canem.databinding.FragmentWalkerSearchBinding
import com.example.canem.network.UpdateWalkBody
import com.example.canem.network.WalkerUpdate
import com.example.canem.preferences.PreferenceProvider
import com.example.canem.ui.main.Status
import com.example.canem.utils.Utils
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.messaging.FirebaseMessaging
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONObject


class WalkerSearchFragment : Fragment(), SensorEventListener , OnMapReadyCallback{
    private lateinit var mapView: MapView

    private var running = false
    private var sensorManager: SensorManager? = null
    private var steps = 0f
    private var latitude: Double = 0.0
    private var longitude: Double = 0.0

    private val LOCATION_PERMISSION_REQUEST = 1
    private var map: GoogleMap? = null
    private var locationReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val b: Bundle? = intent?.extras
            if(b != null){
                latitude = b.getDouble("latitude")
                longitude = b.getDouble("longitude")
                val location = LatLng(latitude,longitude)
                if(map != null){
                    map!!.clear()
                    map!!.addMarker(MarkerOptions().position(location).title("Current walker position"))
                    map!!.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 18f), 4000, null)
                }
            }
        }
    }

    private var isWalkerDataAvailable = false

    private lateinit var binding: FragmentWalkerSearchBinding
    private val walkerVM: WalkerViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentWalkerSearchBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        FirebaseMessaging.getInstance().subscribeToTopic("newWalk")
        sensorManager = activity?.getSystemService(Context.SENSOR_SERVICE) as SensorManager
        binding.apply{
            walkerVM = this@WalkerSearchFragment.walkerVM
            walkerSearchFragment = this@WalkerSearchFragment
        }
        if (Utils.internetConn(requireContext())) {
            walkerVM.getWalker()
            walkerVM.walkerStatus.observe(viewLifecycleOwner, {
                when(it) {
                    Status.SUCCESS -> { isWalkerDataAvailable = true }
                    Status.ERROR -> {Utils.makeToast(requireContext(),"Error getting walker")}
                    else -> {}
                }
            })
        } else {
            Utils.showNeutralDialog(
                requireContext(),
                "Connection Error",
                "You currently don't have internet access. Please try again later.",
                "OK"
            )
        }

        mapView = binding.mapa
        mapView.onCreate(savedInstanceState)
        setMapVisibility(false)
        getLocationAccess()

        binding.buttonStartWalking.setOnClickListener{onStartWalkingButtonClick()}
        binding.buttonEndWalk.setOnClickListener{onEndWalkButtonClick()}
    }

    override fun onStart() {
        super.onStart()
        LocalBroadcastManager.getInstance(requireActivity()).registerReceiver(locationReceiver,
            IntentFilter("LOCATION_UPDATE")
        )
        mapView.onStart()
    }

    override fun onResume() {
        super.onResume()
        initializeStepsSensor()
        mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        updateWalkerStatus(false)
        sensorManager?.unregisterListener(this)
        LocalBroadcastManager.getInstance(requireContext()).unregisterReceiver(locationReceiver)
        requireActivity().stopService(Intent(this.requireContext(),WalkerLocationService::class.java))
        mapView.onPause()
    }

    override fun onStop() {
        super.onStop()
        sensorManager?.unregisterListener(this)
        LocalBroadcastManager.getInstance(requireContext()).unregisterReceiver(locationReceiver)
        requireActivity().stopService(Intent(this.requireContext(),WalkerLocationService::class.java))
        mapView.onStop()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

    override fun onDestroy() {
        super.onDestroy()
        sensorManager?.unregisterListener(this)
        LocalBroadcastManager.getInstance(requireContext()).unregisterReceiver(locationReceiver)
        requireActivity().stopService(Intent(this.requireContext(),WalkerLocationService::class.java))
        mapView.onDestroy()
    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {
    }

    override fun onSensorChanged(event: SensorEvent) {
        if (running) {
            steps ++
            binding.stepsValue.text = "${steps.toInt()}"
        }
    }

    private fun setMapVisibility(visibility: Boolean){
        var buttonVisibilityInt = View.VISIBLE
        var visibilityInt = View.INVISIBLE
        if (visibility) {
            visibilityInt = View.VISIBLE
            buttonVisibilityInt = View.INVISIBLE
        }
        binding.textView.visibility = visibilityInt
        binding.textView2.visibility = visibilityInt
        binding.buttonEndWalk.visibility = visibilityInt
        binding.stepsValue.visibility = visibilityInt
        binding.mapa.visibility = visibilityInt

        binding.buttonStartWalking.visibility = buttonVisibilityInt
    }

    private fun onStartWalkingButtonClick(){
        if (Utils.internetConn(requireContext())) {
            updateWalkerStatus(true)
        } else {
            Utils.showNeutralDialog(
                requireContext(),
                "Connection Error",
                "You currently don't have internet access. Please try again later.",
                "OK"
            )
        }
    }

    private fun onEndWalkButtonClick(){
        if (Utils.internetConn(requireContext())) {
            updateWalkerStatus(false)
            val walkId: String? = PreferenceProvider.getWalkId()
            if (walkId != null) {
                val updateWalkBody = UpdateWalkBody(
                    price = (steps*50),
                    distanceTraveled = steps*0.762.toFloat(),
                    stepsWalked = steps.toInt(),
                    status = 2.0F
                )
                walkerVM.updateWalk(updateWalkBody, walkId)
                walkerVM.updateWalkStatus.observe(viewLifecycleOwner, {
                    when(it) {
                        Status.ERROR -> {Utils.makeToast(requireContext(), "Failed to update walk")}
                        Status.SUCCESS -> {
                            requireContext().packageManager.getApplicationInfo(requireContext().packageName,PackageManager.GET_META_DATA).apply {
                                val serverKey = metaData.getString("FIREBASE_SERVER_KEY")
                                val notification = JSONObject()
                                val notificationBody = JSONObject()
                                notificationBody.put("title", "Walk ended")
                                notificationBody.put("message","The walker has ended the walk.")
                                notificationBody.put("end", true)
                                notification.put("to","/topics/endWalk")
                                notification.put("data",notificationBody)
                                val body = RequestBody.create(MediaType.parse("application/json"),notification.toString())
                                if (serverKey != null) {
                                    walkerVM.sendNotification(body, serverKey)
                                }
                            }
                        }
                        else -> {}
                    }
                })
            }
        } else {
            Utils.showNeutralDialog(
                requireContext(),
                "Connection Error",
                "You currently don't have internet access. Please try again later.",
                "OK"
            )
        }
    }

    private fun initializeStepsSensor(){
        running = true
        val stepsSensor = sensorManager?.getDefaultSensor(Sensor.TYPE_STEP_COUNTER)
        if (stepsSensor == null) {
            Toast.makeText(requireContext(), "No Step Counter Sensor!", Toast.LENGTH_SHORT).show()
        } else {
            sensorManager?.registerListener(this, stepsSensor, SensorManager.SENSOR_DELAY_UI)
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
    }

    private fun getLocationAccess(){
        if (ContextCompat.checkSelfPermission(this.requireContext(), android.Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                this.requireActivity(),
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST)
            return
        }
    }

    private fun updateWalkerStatus(status:Boolean){
        if(isWalkerDataAvailable){
            val walkerResponse = walkerVM.walkerResponse.value
            val walkerID = walkerResponse?.id
            val updatedWalker = WalkerUpdate(
                name = walkerResponse?.name,
                email = walkerResponse?.email,
                uid = walkerResponse?.uid,
                identification = "${latitude};${longitude}",
                rating = walkerResponse?.rating,
                imageURL = walkerResponse?.imageURL,
                status = status,
            )
            if (walkerID != null) {
                walkerVM.updateWalker(updatedWalker, walkerID)
                walkerVM.updateWalkerStatus.observe(viewLifecycleOwner,{
                    when(it){
                        Status.SUCCESS -> {
                            statusUpdatedCallback(status)
                        }
                        Status.ERROR -> {
                            Utils.showNeutralDialog(
                                requireContext(),
                                "Error changing status",
                                "Your status could not be changed. Please try again later.",
                                "OK"
                            )
                        }
                        else -> {}
                    }
                })
            }
        }else{
            Utils.showNeutralDialog(requireContext(),
                "Error changing status",
                "Your data could not be retrieved, so you can't change your status. Please reload this view to try again.",
                "OK")
        }
    }

    private fun statusUpdatedCallback(status: Boolean){
        if(status){
            mapView.getMapAsync(this)
            initializeStepsSensor()
            setMapVisibility(true)
            steps = 0f
            binding.stepsValue.text = "0"
            requireActivity().startService(Intent(requireContext(),WalkerLocationService::class.java))
        }else{
            LocalBroadcastManager.getInstance(requireContext()).unregisterReceiver(locationReceiver)
            requireActivity().stopService(Intent(this.requireContext(),WalkerLocationService::class.java))
            setMapVisibility(false)
            running = false
            sensorManager?.unregisterListener(this)
        }
    }
}