package com.example.canem.nav_fragments.owner

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.example.canem.MainActivity
import com.example.canem.databinding.FragmentOwnerProfileBinding
import com.example.canem.preferences.PreferenceProvider
import com.example.canem.ui.main.MainViewModel


class OwnerProfileFragment : Fragment() {
    private lateinit var binding: FragmentOwnerProfileBinding
    private val viewModel: MainViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentOwnerProfileBinding.inflate(inflater, container, false)
        return binding.root
        //return inflater.inflate(R.layout.fragment_owner_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.apply{
            viewModel = this@OwnerProfileFragment.viewModel
            ownerProfileFragment = this@OwnerProfileFragment
        }
    }

    // AuxMethods

    fun logout() {
        // Clear storage
        viewModelStore.clear()
        PreferenceProvider.clearAll()
        // Go to Login page
        val mainIntent = Intent(requireContext(), MainActivity::class.java)
        startActivity(mainIntent)
        requireActivity().finish()
    }
}