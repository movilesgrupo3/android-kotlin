package com.example.canem.nav_fragments.owner

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.canem.network.ApiServiceInstance
import com.example.canem.network.Breed
import com.example.canem.network.CreateDogBody
import com.example.canem.network.NetworkPet
import com.example.canem.preferences.PreferenceProvider
import com.example.canem.ui.main.Status
import kotlinx.coroutines.launch
import java.lang.Exception

class AddPetViewModel: ViewModel() {
    // State
    private val _breeds = MutableLiveData<List<Breed>>()
    val breeds: LiveData<List<Breed>> = _breeds
    private val _dogResponse = MutableLiveData<NetworkPet>()
    val dogResponse = _dogResponse
    // Status
    private val _breedStatus = MutableLiveData<Status>()
    val breedStatus = _breedStatus
    private val _createDogStatus = MutableLiveData<Status>()
    val createDogStatus = _createDogStatus

    init {
        getBreeds()
    }

    private fun getBreeds() {
        _breedStatus.value = Status.LOADING
        viewModelScope.launch {
            try {
                val resultBreeds = ApiServiceInstance.retrofitService.getAllBreeds()
                _breeds.value = resultBreeds
                _breedStatus.value = Status.SUCCESS
            } catch (e: Exception) {
                Log.e("Error(Get Breeds)", e.message.toString())
                _breedStatus.value = Status.ERROR
                _breeds.value = listOf()
            }
        }
    }

    fun createDog(dog: CreateDogBody) {
        _createDogStatus.value = Status.LOADING
        viewModelScope.launch {
            try {
                val token = PreferenceProvider.getToken()
                val resultDog = ApiServiceInstance.retrofitService
                    .createDog(token = "Bearer $token", dogBody = dog)
                _dogResponse.value = resultDog
                _createDogStatus.value = Status.SUCCESS
            } catch (e: Exception) {
                Log.e("Error(Create Dog)", e.message.toString())
                _createDogStatus.value = Status.ERROR
            }
        }
    }
}