package com.example.canem.nav_fragments.owner

import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.canem.database.getDB
import com.example.canem.domain.Pet
import com.example.canem.network.*
import com.example.canem.repository.CanemRepository
import com.example.canem.preferences.PreferenceProvider
import com.example.canem.ui.main.Status
import kotlinx.coroutines.launch
import okhttp3.RequestBody
import java.lang.Exception

class OwnerViewModel: ViewModel() {
    private val repository = CanemRepository(getDB())

    // State
    val pets = repository.pets
    private val _walkers = MutableLiveData<MutableList<OwnerSearch>>()
    val walkers: LiveData<MutableList<OwnerSearch>> = _walkers
    private val _walks = MutableLiveData<List<Walk>>()
    val walks: LiveData<List<Walk>> = _walks
    private val _favoriteWalkers = MutableLiveData<List<OwnerFavorites>>()
    val favoriteWalkers: LiveData<List<OwnerFavorites>> = _favoriteWalkers
    private val _createWalk = MutableLiveData<Walk>()
    val createWalk: LiveData<Walk> = _createWalk
    // Status
    private val _statusPets = MutableLiveData<Status>()
    val statusPets: LiveData<Status> = _statusPets
    private val _statusWalkers = MutableLiveData<Status>()
    val statusWalkers: LiveData<Status> = _statusWalkers
    private val _statusWalks = MutableLiveData<Status>()
    val statusWalks: LiveData<Status> = _statusWalks
    private val _statusFavoriteWalkers = MutableLiveData<Status>()
    val statusFavoriteWalkers: LiveData<Status> = _statusFavoriteWalkers
    private val _statusCreateWalk = MutableLiveData<Status>()
    val statusCreateWalk: LiveData<Status> = _statusCreateWalk
    private val _statusSendNotification = MutableLiveData<Status>()
    val statusSendNotification: LiveData<Status> = _statusSendNotification

    init {
        getOwnerPets()
        getOwnerWalks()
        getFavoriteWalkers()
    }

    // Aux Methods

    fun setStatusWalkers(status: Status) {
        _statusWalkers.value = status
    }

    fun addDog(dog: Pet) {
        viewModelScope.launch {
            repository.addPet(dog)
        }
    }

    fun deleteDog(dog: Pet) {
        viewModelScope.launch {
            repository.removePet(dog)
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun removeWalker(walkerId: String) {
        _walkers.value?.removeIf { it.id == walkerId }
    }

    private fun getOwnerPets() {
        viewModelScope.launch {
            _statusPets.value = Status.LOADING
            try {
                repository.refreshPets()
                _statusPets.value = Status.SUCCESS
            } catch (e: Exception) {
                Log.e("Error(Get Pets)", e.message.toString())
                if (pets.value.isNullOrEmpty()) {
                    _statusPets.value = Status.ERROR
                } else {
                    _statusPets.value = Status.SUCCESS
                }
            }
        }
    }

    fun getOwnerWalks() {
        _statusWalks.value = Status.LOADING
        viewModelScope.launch {
            try {
                val token = PreferenceProvider.getToken()
                Log.d("TOKEN", token.toString())
                _walks.value = ApiServiceInstance.retrofitService.getOwnerWalks(token = "Bearer $token")
                Log.d("walks", _walks.value.toString())
                _statusWalks.value = Status.SUCCESS
            } catch (e: Exception) {
                Log.e("Error(Get Owner Walks)", e.message.toString())
                _statusWalks.value = Status.ERROR
            }
        }
    }

    fun getNearWalkers(latlng: String) {
        viewModelScope.launch {
            _statusWalkers.value = Status.LOADING
            try {
                val resultWalkers =
                    ApiServiceInstance.retrofitService.getNearWalkers(latlng).toMutableList()
                _walkers.value = resultWalkers
                _statusWalkers.value = Status.SUCCESS
            } catch (e: Exception) {
                Log.e("Error(Get Av.Walkers)", e.message.toString())
                _statusWalkers.value = Status.ERROR
                _walkers.value = mutableListOf()
            }
        }
    }

    private fun getFavoriteWalkers() {
        _statusFavoriteWalkers.value = Status.LOADING
        viewModelScope.launch {
            try {
                val token = PreferenceProvider.getToken()
                Log.d("TOKEN", token.toString())
                _favoriteWalkers.value = ApiServiceInstance.retrofitService.getFavoriteWalkers(token = "Bearer $token")
                Log.d("FAVORITES", _favoriteWalkers.value.toString())
                _statusFavoriteWalkers.value = Status.SUCCESS
            } catch (e: Exception) {
                Log.e("Error(Get Owner Favs)", e.message.toString())
                _statusFavoriteWalkers.value = Status.ERROR
            }
        }
    }

    fun createWalk(createWalkBody: CreateWalkBody) {
        _statusCreateWalk.value = Status.LOADING
        viewModelScope.launch {
            try {
                val createWalkResult = ApiServiceInstance.retrofitService.createWalk(createWalkBody)
                _createWalk.value = createWalkResult
                _statusCreateWalk.value = Status.SUCCESS
            } catch (e: Exception) {
                Log.e("Error(Create Walk)", e.message.toString())
                _statusCreateWalk.value = Status.ERROR
            }
        }
    }

    fun sendNotification(walkNotification: RequestBody, serviceKey: String) {
        _statusSendNotification.value = Status.LOADING
        viewModelScope.launch {
            try {
                FirebaseApiServiceInstance.retrofitService.sendWalkData(
                    "key=$serviceKey",
                    "application/json",
                    walkNotification
                )
                _statusSendNotification.value = Status.SUCCESS
            } catch (e: Exception) {
                Log.e("Error(Create Walk)", e.message.toString())
                _statusSendNotification.value = Status.ERROR
            }
        }
    }
}
