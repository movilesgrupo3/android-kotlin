package com.example.canem.nav_fragments.owner

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.canem.databinding.PetItemBinding
import com.example.canem.domain.Pet
import com.example.canem.network.ApiServiceInstance
import com.example.canem.utils.Utils
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class PetAdapter(fragment: OwnerPetsFragment): ListAdapter<Pet, PetAdapter.PetViewHolder>(DiffCallback) {
    val fragment: OwnerPetsFragment = fragment

    class PetViewHolder(private var binding: PetItemBinding): RecyclerView.ViewHolder(binding.root) {
        val delete: Button = binding.deletePetBtn
        fun bind(pet: Pet) {
            binding.pet = pet
            binding.executePendingBindings()
        }
    }

    // List Adapter methods
    companion object DiffCallback: DiffUtil.ItemCallback<Pet>() {
        override fun areItemsTheSame(oldItem: Pet, newItem: Pet): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Pet, newItem: Pet): Boolean {
            return (
                oldItem.age == newItem.age &&
                oldItem.breedId == newItem.breedId &&
                oldItem.name == newItem.name
            )
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PetAdapter.PetViewHolder {
        return PetViewHolder(
            PetItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: PetAdapter.PetViewHolder, position: Int) {
        val pet: Pet = getItem(position)
        holder.bind(pet)
        holder.delete.setOnClickListener {
            removeDog(pet, it.context)
        }
    }

    // Aux methods
    private fun removeDog(dog: Pet, ctx: Context) {
        // Check internet conn
        var conn: Boolean = Utils.internetConn(ctx)
        if (!conn) {
            Utils.showNeutralDialog(
                ctx,
                "Connection Error",
                "You currently don't have internet access",
                "OK"
            )
            return
        }
        // Prompt confirmation
        MaterialAlertDialogBuilder(ctx)
            .setTitle("Delete Dog")
            .setMessage("Are you sure you want to delete dog ${dog.name}?")
            .setNegativeButton("No"){ _,_ -> }
            .setPositiveButton("Yes"){ _,_ ->
                // Validate conn once again
                conn = Utils.internetConn(ctx)
                if (!conn) {
                    Utils.showNeutralDialog(
                        ctx,
                        "Connection Error",
                        "You currently don't have internet access",
                        "OK"
                    )
                    return@setPositiveButton
                }
                // Attempt dog removal
                runBlocking {
                    launch {
                        try {
                            ApiServiceInstance.retrofitService.deleteDog(dog.id) // Delete request
                            // Update UI
                            fragment.deleteDog(dog)
                            notifyDataSetChanged()
                            // On success dialog
                            Utils.showNeutralDialog(
                                ctx,
                                "Delete Dog",
                                "Dog ${dog.name} deleted successfully",
                                "OK"
                            )
                        } catch (e: Exception) {
                            Log.e("Error(Del Dog)", e.toString())
                            Utils.showNeutralDialog(
                                ctx,
                                "Delete Dog Error",
                                "Failed to delete dog",
                                "OK"
                            )
                        }
                    }
                }
            }
            .show()
    }
}