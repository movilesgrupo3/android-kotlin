package com.example.canem.nav_fragments.owner

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import com.example.canem.databinding.FragmentOwnerHistoricalBinding
import com.example.canem.ui.main.Status
import com.example.canem.utils.Utils
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import java.text.SimpleDateFormat
import java.util.*


class OwnerHistoricalFragment : Fragment() {
    private lateinit var binding: FragmentOwnerHistoricalBinding
    private val ownerVM: OwnerViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?)
            : View? {
        binding = FragmentOwnerHistoricalBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // View & Data binding
        binding.lifecycleOwner = this
        binding.viewModel = ownerVM
        binding.recyclerView.adapter = OwnerHistoricalAdapter()
        // LastWalkDate check after getting the walks
        ownerVM.statusWalks.observe(viewLifecycleOwner, {
            when(it) {
                Status.SUCCESS -> { createWalksDialog() }
                Status.ERROR -> {makeToast("Error getting walks")}
                else -> {}
            }
        })
        // Set refresh
        binding.swipeRefresh.setOnRefreshListener {
            // Check conn
            if (!Utils.internetConn(requireContext())) {
                Utils.showNeutralDialog(
                    requireContext(),
                    "Connection Error",
                    "You currently don't have internet access",
                    "OK"
                )
                binding.swipeRefresh.isRefreshing = false
                return@setOnRefreshListener
            }
            // On refresh
            ownerVM.getOwnerWalks()
            ownerVM.statusWalks.observe(viewLifecycleOwner, {
                when(it) {
                    Status.LOADING -> {binding.swipeRefresh.isRefreshing = true}
                    Status.ERROR -> {
                        binding.swipeRefresh.isRefreshing = false
                        Utils.makeToast(requireContext(), "Failed to fetch walks")
                    }
                    else -> {binding.swipeRefresh.isRefreshing = false}
                }
            })
        }
    }

    fun makeToast(text: String) {
        Toast.makeText(requireContext(), text, Toast.LENGTH_LONG).show()
    }

    fun createWalksDialog(){
        val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
        val dateCurrent = Calendar.getInstance().time
        if(ownerVM.walks.value.isNullOrEmpty()){
            MaterialAlertDialogBuilder(requireContext())
                .setTitle("This looks empty...")
                .setMessage("It seems like you haven't requested a walk yet. Would you like to request a walk now?")
                .setNegativeButton("I am not interested") { dialog, which ->
                }
                .setPositiveButton("Sure!") { _,_ ->
                    // Respond to positive button press
                }
                .show()
        }else{
            val dateRaw = ownerVM.walks.value?.get(0)?.date
            val dateLastWalk = format.parse(dateRaw)
            val diff: Long = dateCurrent.time - dateLastWalk.time
            val seconds = diff / 1000
            val minutes = seconds / 60
            val hours = minutes / 60
            val days = hours / 24
            if (days >= 3){
                MaterialAlertDialogBuilder(requireContext())
                    .setTitle("Do you want to request a walk for your pets?")
                    .setMessage("You haven't asked your dogs for a walk for more than 3 days. You should consider doing it soon, as it could benefit their health.")
                    .setNegativeButton("I am not interested") { dialog, which ->
                    }
                    .setPositiveButton("Sure!") { dialog, which ->
                        // Respond to positive button press
                    }
                    .show()
            }
        }
    }
}
