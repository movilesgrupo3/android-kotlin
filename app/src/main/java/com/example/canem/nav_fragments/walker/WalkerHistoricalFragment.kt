package com.example.canem.nav_fragments.walker

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.example.canem.databinding.FragmentWalkerHistoricalBinding

class WalkerHistoricalFragment : Fragment() {
    private lateinit var binding: FragmentWalkerHistoricalBinding
    private val walkerVM: WalkerViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentWalkerHistoricalBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = this
        binding.viewModel = walkerVM
        binding.recyclerView.adapter = WalkerHistoricalAdapter()
    }
}