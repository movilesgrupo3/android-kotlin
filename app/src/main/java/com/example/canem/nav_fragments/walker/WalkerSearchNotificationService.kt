package com.example.canem.nav_fragments.walker
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.example.canem.R
import com.example.canem.nav_fragments.owner.MyBroadcastReceiverReceiver
import com.example.canem.preferences.PreferenceProvider
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import java.util.*

class WalkerSearchNotificationService: FirebaseMessagingService() {
    private val ADMIN_CHANNEL_ID = "admin_channel"
    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.common_google_play_services_notification_channel_name)
            val descriptionText = getString(R.string.common_google_play_services_notification_ticker)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(ADMIN_CHANNEL_ID, name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private fun setupChannels(notificationManager: NotificationManager?) {
        val adminChannelName = "New notification"
        val adminChannelDescription = "Device to device notification"

        val adminChannel: NotificationChannel
        adminChannel = NotificationChannel(ADMIN_CHANNEL_ID, adminChannelName, NotificationManager.IMPORTANCE_HIGH)
        adminChannel.description = adminChannelDescription
        adminChannel.enableLights(true)
        adminChannel.lightColor = Color.RED
        adminChannel.enableVibration(true)
        notificationManager?.createNotificationChannel(adminChannel)
    }

    override fun onMessageReceived(p0: RemoteMessage) {
        Log.d("Recibido", "")
        super.onMessageReceived(p0)
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val notificationID = Random().nextInt(3000)
        val intent = Intent(this, WalkerSearchFragment::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

        val activityActionPendingIntent: PendingIntent =
            PendingIntent.getActivity(application, 0, intent, 0)

        // adding action for broadcast
        val broadcastIntent = Intent(application, MyBroadcastReceiverReceiver::class.java).apply {
            putExtra("action_msg", "some message for toast")
        }
        val broadcastPendingIntent: PendingIntent =
            PendingIntent.getBroadcast(application, 0, broadcastIntent, 0)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            setupChannels(notificationManager)
        }

        Log.d("messageReceived", p0.toString())
        Log.d("titulo", p0.data.get("title").toString())
        Log.d("mensaje", p0.data.get("message").toString())

        if (p0.data.get("end").toBoolean()){
            val notificationBuilder = NotificationCompat.Builder(this, ADMIN_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_walk)
                .setContentTitle(p0?.data?.get("title"))
                .setContentText(p0?.data?.get("message"))
                .setAutoCancel(true)
                .addAction(R.drawable.ic_walk, "Rate Walk", broadcastPendingIntent)
            notificationManager.notify(notificationID, notificationBuilder.build())
        }
        else{
            val notificationBuilder = NotificationCompat.Builder(this, ADMIN_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_walk)
                .setContentTitle(p0?.data?.get("title"))
                .setContentText(p0?.data?.get("message"))
                .setAutoCancel(true)
            notificationManager.notify(notificationID, notificationBuilder.build())
        }

        // Add walkId to preferences
        p0.data.get("walkID")?.let { PreferenceProvider.setWalkId(it) }
    }


}
