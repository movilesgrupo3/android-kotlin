package com.example.canem.nav_fragments.walker

import android.app.Service
import android.content.Context
import android.content.Intent
import android.location.Location
import android.os.IBinder
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.example.canem.utils.Utils

class WalkerLocationService: Service(), LocationListener {

    private lateinit var mContext: Context

    private var isGPSEnabled = false
    private var isNetworkEnabled = false
    private var location:Location? = null

    private var locationManager: LocationManager? = null
    private var intent: Intent = Intent("LOCATION_UPDATE")

    override fun onBind(intent: Intent): IBinder? {
        throw UnsupportedOperationException("Not yet implemented")
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        this.mContext = this
        getLocation()
        return super.onStartCommand(intent, flags, startId)
    }

    private fun getLocation(): Location? {
        try {
            locationManager = mContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager

            isGPSEnabled = locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)
            isNetworkEnabled = locationManager!!.isProviderEnabled(LocationManager.NETWORK_PROVIDER)

            if (!isGPSEnabled && !isNetworkEnabled){
                Utils.showNeutralDialog(applicationContext,
                    "GPS not enabled",
                    "It seems like your GPS is disabled. Please enable it to use this feature.",
                "OK")
            }else{
                if(isNetworkEnabled){
                    locationManager!!.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 500L, 1f, this)
                    location = locationManager!!.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
                    if(location != null){
                        onLocationChanged(location!!)
                    }
                }else if(isGPSEnabled){
                    if(location == null){
                        locationManager!!.requestLocationUpdates(LocationManager.GPS_PROVIDER,500L,1f,this)
                        location = locationManager!!.getLastKnownLocation(LocationManager.GPS_PROVIDER)
                        if(location != null){
                            onLocationChanged(location!!)
                        }
                    }
                }
            }

        }catch (e: SecurityException){
            Utils.makeToast(applicationContext,"Error retrieving location")
        }
        return location
    }

    private fun stopUsingGPS(){
        if(locationManager != null){
            locationManager!!.removeUpdates(this)
        }
    }

    override fun onDestroy() {
        stopUsingGPS()
        stopSelf()
        super.onDestroy()
    }

    override fun onLocationChanged(p0: Location) {
        val b = Bundle()
        b.putDouble("latitude",p0.latitude)
        b.putDouble("longitude",p0.longitude)
        intent.putExtras(b)
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent)
    }
}