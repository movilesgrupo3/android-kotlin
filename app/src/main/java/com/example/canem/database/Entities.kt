package com.example.canem.database

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.canem.domain.Pet

// PET
@Entity
data class PetEntity constructor(
    @PrimaryKey
    val id: String,
    val name: String,
    val age: Int,
    val imageURL: String?,
    val breedId: String,
    val ownerId: String
)

fun MutableList<PetEntity>.asDomainModel(): MutableList<Pet> {
    return map {
        Pet(
            id = it.id,
            name = it.name,
            age = it.age,
            imageURL = it.imageURL,
            breedId = it.breedId,
            ownerId = it.ownerId
        )
    }.toMutableList()
}