package com.example.canem.database

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.room.Dao

@Dao
interface CanemDao {
    @Query("select * from petentity")
    fun getPets(): LiveData<MutableList<PetEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(pets: MutableList<PetEntity>)

    @Query("select * from petentity where ownerId = :ownerId")
    fun getOwnerPets(ownerId: String): LiveData<MutableList<PetEntity>>

    @Insert
    fun insertOne(pet: PetEntity)

    @Delete
    fun deleteOne(pet: PetEntity)
}

@Database(entities = [PetEntity::class], version = 1, exportSchema = false)
abstract class CanemDB: RoomDatabase() {
    abstract val canemDao: CanemDao
}

private lateinit var INSTANCE: CanemDB
private lateinit var ctx: Context

fun initDB(appCtx: Context) {
    ctx = appCtx
}

fun getDB(): CanemDB {
    synchronized(CanemDB::class.java) {
        if (!::INSTANCE.isInitialized) {
            INSTANCE = Room.databaseBuilder(
                ctx.applicationContext,
                CanemDB::class.java,
                "canem"
            )
                .fallbackToDestructiveMigration()
                .build()
        }
    }
    return INSTANCE
}
